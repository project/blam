<?php

/**
 * @file
 * Defines the admin interface for blam.
 */

/**
 * Defines the blam admin settings form.
 */
function blam_settings() {
  $form = array();

  $form['blam_restrict'] = array(
    '#title' => t('Restricted Paths'),
    '#description' => t('Provide a comma-separated list of paths you want
      checked for excess arguments.'),
    '#type' => 'textarea',
    '#default_value' => implode(',', variable_get('blam_restrict', array())),
  );
  $form['blam_submit'] = array(
    '#value' => t('Submit'),
    '#type' => 'submit',
  );

  return $form;
}

/**
 * BLAM settings form submit handler.
 */
function blam_settings_submit($form, &$form_state) {
  $blam_restrict = check_plain($form_state['values']['blam_restrict']);

  $orig_restricted_paths = explode(',', $blam_restrict);
  $restricted_paths = array();

  foreach ($orig_restricted_paths as $path) {
    $restricted_paths[] = trim($path);
  }

  variable_set('blam_restrict', $restricted_paths);
}
